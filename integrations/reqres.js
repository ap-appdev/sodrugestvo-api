const axios = require('axios');

const ReqRes = {
	async getUsers() {
		const usersData = await this.getUsersByPage(1)
		const users = usersData.data
		if(usersData.total_pages > 1) {
			const pages = [];
			for(let page = 2; page <= usersData.total_pages; page++) {
				pages.push(page)
			}
			for await (let page of pages.map(this.getUsersByPage)) {
				users.push(...page.data)
			}
		}
		return users
	},
	async getUsersByPage(page) {
		return (await axios.get(`https://reqres.in/api/users?page=${page}`)).data
	}
}

module.exports = ReqRes;
