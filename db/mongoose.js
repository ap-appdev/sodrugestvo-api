const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017', {
  useCreateIndex: true,
  useUnifiedTopology: true,
  useNewUrlParser: true
}).then(() => {
  console.info('Connected to DB')
}).catch((err) => {
  console.error('Connection error:', err.message)
})

module.exports = mongoose
