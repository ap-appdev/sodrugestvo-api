const User = require('../models/User');

const controller = {
	users: async (req, res) => {
		req.query.search = req.query.search || ''
		const search = req.query.search.split(' ')
		const users = await User.find({
			$or: [
				{
					first_name: { $regex: search[0] || '', $options : '-i' },
					last_name: { $regex: search[1] || '', $options : '-i' }
				},
				{
					first_name: { $regex: search[1] || '', $options : '-i' },
					last_name: { $regex: search[0] || '', $options : '-i' }
				}
			]
		})
		return res.json(users)
	}
}

module.exports = (method) => {
	return async function (request, response, next) {
		try {
			await controller[method](request, response, next)
		} catch (error) {
			return next(error)
		}
	}
}
