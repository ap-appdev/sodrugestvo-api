const CronJob = require('cron').CronJob;
const reqres = require('../integrations/reqres')
const User = require('../models/User');

new CronJob('0 */1 * * * *', async () => {
	const users = await reqres.getUsers()
	await User.deleteMany({
		id: {
			$nin: users.map(item => item.id)
		}
	})
	await Promise.all(users.map(async user => {
		await User.replaceOne({ id: user.id }, user,{ upsert: true })
	}))
}, null, true);
