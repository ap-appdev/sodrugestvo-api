const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema({
  id: Number,
  first_name: String,
  last_name: String,
  email: {
    type: String,
    unique: true,
    required: true
  },
  avatar: String
})

module.exports = mongoose.model('User', User)
